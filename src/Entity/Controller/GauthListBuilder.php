<?php

namespace Drupal\gauth\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for gauth entity.
 *
 * @ingroup gauth
 */
class GauthListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Gauth implements a Gauth account model. These gauth accounts are fieldable entities. You can manage the fields on the <a href="@adminlink">Gauth admin page</a>.', [
        '@adminlink' => \Drupal::urlGenerator()
          ->generateFromRoute('gauth.gauth_settings'),
      ]),
    ];
    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the snapshot list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header = ['Id', 'Name', 'Services', 'Is Authenticated'];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\gauth\Entity\Gauth */

    $row = [
      'id' => $entity->getId(),
      'Name' => $entity->getName(),
      'Services' => implode(", ", gauth_google_services_names([], [], [], $entity->getServices())),
      'is_authenticated' => $entity->getAuthenticated() ? t('Yes') : t('No'),
    ];
    return $row + parent::buildRow($entity);
  }

}
