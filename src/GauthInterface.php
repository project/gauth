<?php

namespace Drupal\gauth;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Gauth entity.
 *
 * @ingroup gauth
 */
interface GauthInterface extends ContentEntityInterface, EntityOwnerInterface {

}
