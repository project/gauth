<?php

namespace Drupal\gauth\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a form for deleting a gauth entity.
 *
 * @ingroup gauth
 */
class GauthDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return 'Are you sure you want to delete this account';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return "This account will be deleted from the system and won't be available";
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return "Delete";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.gauth.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gauth = $this->entity;
    $gauth->delete();
    parent::submitForm($form, $form_state);
    \Drupal::messenger()->addMessage('Gauth account deleted successfully');
    $response = new RedirectResponse('/admin/config/services/gauth');
    $response->send();
  }

}
