<?php

namespace Drupal\gauth\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\gauth\Entity\Gauth;

/**
 * Provides a form for revoking a gauth entity.
 *
 * @ingroup gauth
 */
class GauthRevokeForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return 'Are you sure you want to revoke access token of this account';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return "This account can't be used for api call until authenticated again";
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return "Revoke";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.gauth.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gauth = $this->entity;
    $client = Gauth::getGauthClient($gauth);
    $client->revokeToken();
    $gauth->setAccessToken('{}');
    $gauth->setAuthenticated(FALSE);
    $gauth->save();
    parent::submitForm($form, $form_state);
    \Drupal::messenger()->addMessage('Gauth account revoked successfully');
    $response = new RedirectResponse('/admin/config/services/gauth');
    $response->send();
  }

}
