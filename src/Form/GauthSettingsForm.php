<?php

namespace Drupal\gauth\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\gauth\Form
 *
 * @ingroup gauth
 */
class GauthSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'gauth_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    _gauth_read_scope_info();
  }

  /**
   * Define the form used for ContentEntityExample settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['gauth_settings']['#markup'] = 'Settings form for Gauth. Manage field settings here.<br/><br/>';
    $form['gauth_intro']['#markup'] = "Gauth tries to detect all supported services and scopes from the library installed. <br/> If you don't find your desired class/service listed or you have updated library you need to flush cache or hit 'Scan library' button, to see them here.<br/>";
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Scan Library'),
    ];

    $names = \Drupal::config('gauth.google_api_services')->get('gauth_google_api_services');
    $form['gauth_services'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#title' => 'The supported services are:',
      '#items' => $names,
    ];
    return $form;
  }

}
