<?php

namespace Drupal\gauth\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the gauth entity edit forms.
 *
 * @ingroup gauth
 */
class GauthForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\gauth\Entity\Gauth */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      \Drupal::messenger()->addMessage(t('The gauth %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));
    }
    else {
      \Drupal::messenger()->addMessage(t('The gauth %feed has been added. You now need to authenticate this new account.', ['%feed' => $entity->toLink()->toString()]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

}
